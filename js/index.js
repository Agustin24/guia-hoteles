$(function(){

    $("[data-toggle='tooltip']").tooltip();

    $("[data-toggle='popover']").popover();

    $('.carousel').carousel({
        interval: 1000
    })
    $('#contacto').on('show.bs.modal', function (e) {
        console.log('mostrando modal')
        $('#btnContacto').removeClass('btn-outline-success');
        $('#btnContacto').addClass('btn-primary');
        $('#btnContacto').prop('disabled', true);
    });
    $('#contacto').on('shown.bs.modal', function (e) {
        console.log('se mostro modal')
    });
    $('#contacto').on('hide.bs.modal', function (e) {
        console.log('se oculta modal')
    });
    $('#contacto').on('hidden.bs.modal', function (e) {
        console.log('se oculto modal')
        $('#btnContacto').prop('disabled', false);
        $('#btnContacto').addClass('btn-outline-success');
    });
}); 
